import * as React from 'react';
import {View, StyleSheet, Text, Alert} from 'react-native';

import {GLView} from 'expo-gl';
//import {THREE} from 'expo-three';
//import ExpoTHREE from 'expo-three';

import * as THREE from 'three';
//import {THREE} from 'expo-three';


import { DDSLoader } from 'three/examples/js/loaders/DDSLoader.js';
import { MTLLoader } from 'three/examples/js/loaders/MTLLoader.js';
import { OBJLoader } from 'three/examples/js/loaders/OBJLoader.js';


import * as makehuman from 'makehuman-js';
//import {Human} from './src/index'

export default class ThreeHuman extends React.Component {
  componentDidMount() {
    //THREE.suppressExpoWarnings(true);
  }

  constructor(props) {
    super(props);
    //this._onGLContextCreate = this._onGLContextCreate.bind(this);
    //this.animate = this.animate.bind(this);
  }

  _onGLContextCreateSec = async gl => {
    //console.log(document);

    const {drawingBufferWidth: width, drawingBufferHeight: height} = gl;


      // Instantiate a loader
      var loader = new THREE.GLTFLoader();

// Optional: Provide a DRACOLoader instance to decode compressed mesh data
      var dracoLoader = new THREE.DRACOLoader();
      dracoLoader.setDecoderPath( '/examples/js/libs/draco/' );
      loader.setDRACOLoader( dracoLoader );

// Load a glTF resource
      loader.load(
          // resource URL
          'http://sroprosper.qwerbit.ru/test/test.obj',
          // called when the resource is loaded
          function ( gltf ) {

              scene.add( gltf.scene );

              gltf.animations; // Array<THREE.AnimationClip>
              gltf.scene; // THREE.Scene
              gltf.scenes; // Array<THREE.Scene>
              gltf.cameras; // Array<THREE.Camera>
              gltf.asset; // Object

          },
          // called while loading is progressing
          function ( xhr ) {

              console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

          },
          // called when loading has errors
          function ( error ) {
              console.log( 'An error happened' );
          }
      );

    gl.endFrameEXP();
  };

  render() {
    return (
      <View style={styles.container}>
        <GLView
          style={{width: 300, height: 300}}
          onContextCreate={this._onGLContextCreateSec}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    //backgroundColor: '#ecf001',
    padding: 8,
  },
});
