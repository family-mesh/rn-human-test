import * as React from 'react';
import {View, StyleSheet, Text, Alert} from 'react-native';

import {GLView} from 'expo-gl';
//import {THREE} from 'expo-three';
//import ExpoTHREE from 'expo-three';
import * as THREE from 'three';
import {Human} from './src/index'
import man from './men.json';

export default class ThreeHuman extends React.Component {
  componentDidMount() {
    //THREE.suppressExpoWarnings(true);
  }

  constructor(props) {
    super(props);
    //this._onGLContextCreate = this._onGLContextCreate.bind(this);
    //this.animate = this.animate.bind(this);
  }

  _onGLContextCreateSec = gl => {

    const human3 = new Human(
        'http://sroprosper.qwerbit.ru/assets/makehuman-data/public/data/resources.json',
        'http://sroprosper.qwerbit.ru/assets/makehuman-data/src/json/sliders/modeling_sliders.json',
        'http://sroprosper.qwerbit.ru/assets/makehuman-data/public/data/'
    );
    human3.createHuman().then(() => {
      //const container = document.getElementById('container3');
      // human3.setPose('standing03');
      // human3.setProxy('Low-Poly');
      /// human3.setProxy('Braid01');
      // human3.setProxy('Cocktaildress');

      // human3.setColor();
      human3.createScene(gl);
      human3.addHumanToScene();
      human3.setCameraPosition(0,14,20);
      human3.onResize();
    })
  };

  render() {
    return (
      <View style={styles.container}>
        <GLView
          style={{width: 300, height: 300}}
          onContextCreate={this._onGLContextCreateSec}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    //backgroundColor: '#ecf001',
    padding: 8,
  },
});
